--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import qualified Data.Set as S
import           Text.Pandoc.Options


--------------------------------------------------------------------------------

customPandocCompiler :: Compiler (Item String)
customPandocCompiler =
    let customExtensions = extensionsFromList [Ext_footnotes]
        defaultExtensionsW = writerExtensions defaultHakyllWriterOptions
        defaultExtensionsR = readerExtensions defaultHakyllReaderOptions
        newExtensionsW = defaultExtensionsW <> customExtensions
        newExtensionsR = defaultExtensionsR <> customExtensions
        writerOptions = defaultHakyllWriterOptions {
                          writerExtensions = newExtensionsW
                        }
        readerOptions = defaultHakyllReaderOptions {
                          readerExtensions = newExtensionsR
                        }
    in pandocCompilerWith readerOptions writerOptions

main :: IO ()
main = hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "home.md" $ do
        route $ constRoute "index.html"
        compile $ customPandocCompiler
            -- >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match "gdp.rst" $ do
        route $ constRoute "gdp.html"
        compile $ customPandocCompiler
            -- >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext
