--- 
title: Home
date: 2020-1-1 
---

Do you wish there was a way to safely create and share mutable variables? Do you
wish you could ensure all open files were eventually closed and that once
closed, they were never used again? Have you ever wanted fight the Rust borrow
checker in your Haskell code? Do you want something battle tested and production
ready?

This library can give all of that [^1] and more while providing an end user
interface that looks remarkably normal!

Real linear types are close, but aren't yet in GHC. That doesn't mean you can't
try to capture some of their benefits today. All you need is GHC 8.6.5 and this
codebase.

Here are some code
snippets to give you a taste of what's possible:

```haskell

    example = do
      f1 <- openFile "test.txt"
      f2 <- openFile "testt.txt"

      c1 <- readFile f2
      c12 <- readFile f2
      c2 <- readFile f1

      closeFile $ f2

      -- Uncomment the following line to get a type error
      -- c2 <- readFile f2

      -- Comment the following like to get a type error
      closeFile $ f1

      -- Uncomment the following line to get a type error
      -- closeFile $ f2
  
    rust = do
      -- Create a immutable variable called a
      a <- let' @NotMutable 5

      -- Create a mutable variable called b
      b <- let' @Mutable 6

      -- Change the value in b
      b =. 7

      -- Uncomment the following line to get a type error
      -- a =. 7

      -- Add a and b moving them into the plus function
      c <- plus a b

      -- Uncomment to get a type error.
      -- You can't drop what's already been moved.
      -- drop' a

      d <- move c

      -- Make immutable references to d
      rd <- mkRef $ d
      rc <- mkRef $ d

      -- Uncomment the following line to get a type error
      -- You can't make a mutable reference if immutable ones exist
      -- rmc <- mkMutRef $ d

      -- Uncomment the following line to get a type error
      -- You can't drop d because it has references to it.
      -- drop' d

      -- Unfortunately, dropping is kind of manual unlike in Rust.
      dropRef' rc
      dropRef' rd

      -- Return the contents of d. Unsafe because the unwrapped value
      -- could be saved and modified in ways that break linear types.
      -- We'll just assume we won't do anything bad like that with the value.
      unsafeUnOwn d
```

Before you add this code to your mission critical mainframe, there are a few
concerns I should mention. First, the error messages are disgusting. When things
go wrong, GHC tends to mark seemingly random lines with errors that leak all
sorts of implementation details. Second, it uses some questionable features
behind the scenes. To get that nice do notation syntax, you need to enable the
``RebindableSyntax`` extension. More worryingly, you also have to enable
``ImpredicativeTypes`` which, according to the GHC user's guide, is "considered
highly experimental, and certainly un-supported". Worst of all, the Wiki page
link for that extension doesn't work.

With that said, this was a lot of fun to implement and required jumping into
topics I otherwise might not have explored. Because of that, I've decided to
write up a little guide about how I created this code. There's a pretty good
chance someone will come around and find an example that breaks everything, but
the fact that the examples above even work is fascinating to me.

I've broken the description up into several pieces. They assume some prior
knowledge of Haskell although they try to motivate all of the design decisions.

[First up is the one on the Ghosts of Departed Proofs Paper.](/gdp.html)


[^1]: Except the last one. That one's not even close.
