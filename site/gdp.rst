A lot of the inspiration for codebase came from a great paper called Ghosts of
Departed Proofs and the associated ecosystem around it.  If you haven't, I would
recommend reading a blog post or two about it and maybe the paper itself. What
follows is my own summary of the paper and some of the motivations that lead to
it.

There are two really important ideas we'll be exploring: language features that would
require complicated types can be replaced with carefully managed modules, and
proofs about types can be tagged with a phantom type parameter to give them
meaning. I'll try explaining both ideas here, so if you already feel
comfortable with the paper, you can skip this.

Modules and Smart Constructors
==============================

Modules might not have the fascinating theoretical foundation other features in
Haskell have, but they are still extremely powerful. For example, imagine a
library which needs to have it's ``initialize`` function called before any
other. One option would be to document that dependency and throw an error if it
isn't met. Although technically viable, it doesn't seem to be the "Haskell Way".
Instead, it would be great if we could prevent the user from even calling any
other functions without first calling ``initialize``.

One way to accomplish that is to define a type ``data IsInitialized =
IsInitialized``. We can then give ``initialize`` a type signature like
``initialize :: Config -> IO IsInitialized``. Then, every function called
afterwards can take an ``IsInitialized`` as a parameter. It's now impossible for
the user to call such a function without first calling ``initialize``. Well,
kind of. What if the user just used the constructor for ``IsInitialized`` and
made their own instance?

Luckily, we can prevent them from doing that. Haskell gives us plenty of control
over what we export. We can export the ``IsInitialized`` type but not the
constructor.  This can be thought of as roughly equivalent to making the
``IsInitialized`` constructor private in an object oriented language.

Of course this still isn't ideal. It's kind of annoying having to pass around a
"useless" variable to every function that depends on ``initialized``. For now,
just keep this annoyance in the back of your mind as we explore other topics.

Phantom Types
=============

Let's jump to the other GDP topic: phantom types. Phantom types are easy to
describe, but why they're useful isn't always obvious. A normal data type like
``data Maybe a = Just a | Nothing`` has a type parameter ``a`` which is created
on the left side of the equal sign and used on the right. This lets us create,
for example, containers for other types data.

A phantom, on the other hand, only creates a type parameter but doesn't use it
on the right hand side. An example would be ``data Proxy a = Proxy``. What's the
point of this type? Since the type parameter doesn't show up on the right, we
can assume it has no impact on runtime. This means it's only useful at compile
time.

Proxy in particular has it's uses in the base libraries, but imagine for a
moment that, in a module, we only exported the ``Proxy`` type, not the
constructor.  What if we replaced the type signature of ``initialize`` with
``initialize :: Config -> IO (Proxy IsInitialized)``. Now, we can safely export
all of ``IsInitialized`` and reuse the Proxy variable. Assuming the user doesn't
use coerce, we've got the exact same guarantees as before but with a more
complicated piece of code. Instead of turning back to the simpler solution,
let's run forward and make things even more complicated.

What's the use of a type parameter? Basically, it's to define something that
works over any type. For example, ``Maybe`` doesn't, and shouldn't, be able to
assume anything about the type that's stored inside it. Instead the caller has
control over what's stored in the ``Maybe``. Here, we run into an extremely
important distinction. The caller and callee have different levels of power.
The caller can pick the type while the callee can't say anything about the type.
What if we reversed those roles?

Let's work backwards on this one. Assuming the callee can pick the type and
the caller can't, can we create something useful?

Existential Types
=================

The short answer is yes: we've just created something called existential types.
From the caller's point of view, the only thing they know about the type is that
it exists. They have no idea what it is though. The callee on the other hand can
pick exactly the what type they want it to be.

In the case of Maybe, this seems pretty useless; imagine if our container picked
what type was stored inside it and we just had to deal with it. That seems to go
against the entire point of a container. Maybe though, we can get
something more useful out of ``Proxy``. When making ``Proxy`` existential,
nothing immediately jumps out as being a problem, although the benefits aren't
necessarily clear either. If haven't made self-aware containers, then what have
we made?

Looking back at non-existential types (called universal types) we find that it's
not entirely true that the callee/caller relationship is completely binary. Instead,
we can use things like type classes to leak some info into the callee. Likewise,
the caller finds itself giving up some of its power when using type classes.

Type classes and constraints can be used in a similar way on existential
relationships; information can be leaked from the callee back to the caller.
The standard example is an existential container of things that implement
``Show``. We don't know what's in the container, but we can call ``show`` on it
to get a string. Don't worry though, we'll be using existential types for much
more interesting stuff.

Proofs
======

At this point we have almost unraveled the key ideas in Ghosts of Departed
Proofs. Only one small leap remains. Moving our initialization example to the
side, let's instead imagine a division function. Here we have a similar
problem: our code can throw an exception when it's given 0, but it would be
better if the user just couldn't pass it 0.

One popular idea which the authors refute, is that the best way to handle that
is to return a ``Maybe`` value. This tends to lead to frustration. Imagine if
division had the signature ``(/) :: Double -> Double -> Maybe Double``. Add to
your imagination some number ``n`` which we "know" can't be 0 and needs to be
the divisor for several equations. Now you have to unwrap a Maybe for each of
those types. If you were using this division function, you would probably be
tempted to always compose it with a call to ``fromJust``. At that point, you've
thrown type safety out the window.

Another option is to create a wrapper type around numbers which only exports a
smart constructor. Technically, someone could put 0 in the wrapper, but our
smart constructor should prevent that assuming it's coded correctly and we
haven't exported the original constructor. An example of this can
be seen in the ``NonEmpty`` type in Haskell's base.

Unfortunately, we don't have all of the power we'd really like. Part of what
makes NonEmpty useful is how it uses ADT's to make representing invalid states
impossible. Not all conditions are so easily encoded. What if we wanted to
represent the condition that a list is sorted. How can you use ADT's to assert
that?

We could try to create types that meet our conditions, but what if we took a
step back and did something less formal. Why do we need to encode our condition
as an ADT when we have smart constructors and names? For example, we could
"assert" that a list is sorted by creating a type ``newtype Sorted a = Sorted
[a]`` and although the compiler won't help us much in the case our smart
constructor is wrong, it's really not that bad. We could use tests to "prove"
that our constructors are valid and the type system to prove that the user's
code is valid. Of course, this still has its flaws. Imagine needing two sorted
lists of the same size. We could define the size constraint as ``data SameSize a
= SameSize [a] [a]`` but how do we compose the conditions? We could probably do something
with ``Coercible``, but that's not the solution we're looking at today.

Let's break down what our wrappers really are. If we are holding something
wrapped in Sorted, we are asserting to the user that the thing stored inside is
indeed sorted. We're using the name ``Sorted`` to tell the user what the
condition is. We're using the wrapping relationship to specify what is being
sorted. Names are working pretty well, but wrapping is creating some problems.
Can we avoid wrapping our data without losing the relationship?

First, we'll create a type ``data Wrapped s a = Wrap a``. Next, we'll modify our
``Sorted`` type to be ``newtype Sorted s = Sorted``. Now, we'll assert that if
the ``s`` variable on a Wrapped is equal to the ``s`` variable on a ``Sorted``,
the two are related. For example, we could define a function like ``binSearch ::
a -> (Wrapped s [a], Sorted s) -> Maybe a``. This function says that given
something of type ``a`` to look for and a sorted list of ``a``'s, we might
return some element ``a``. Our sorted lists that had to be the same size could
be represented as ``someFunction :: (Wrapped s1 [a], Sorted s1) -> (Wrapped s2
[a], Sorted s2) -> SameSize s1 s2 -> ...``. Sure, it's still kind of annoying
that we have to actually pass around proofs, but this isn't too bad. Or at
least it would be if the caller of ``someFunction`` couldn't pick the types of
``s1`` and ``s2``.  The user could pick an ``s`` for ``Wrapped`` which happened
to match an existing ``Sorted`` instance. A malicious user can always do whatever they
want to break us so you might argue that that's fine. Unfortunately, an
imperfect user might accidentally pass the wrong parameter and the type system
will actually help them line up the types to break us. If we were allowed to assume our
library users were perfect, we could have given up hours ago!

Luckily, we have a way of taking control from the caller and giving it to the
callee: existential types! Because of limitations in Haskell's type system, we
have to create a slightly weird looking smart constructor for ``Wrapped``:
``wrap :: a -> (forall s. Wrapped s a -> r) -> r``. Basically, the user is going
to give ``wrap`` the type they want wrapped as well as what they want to do with
the wrapped value. We have to use the continuous passing style because Haskell
only let's us create type variables using ``forall`` when what we really want is
something like ``exists``. If you think the ``wrap`` function looks like of
gross, don't worry. Soon we'll abuse the compiler into letting us more
accurately emulate existential types.

By hiding the ``Wrap`` constructor and only exporting the ``wrap`` smart
constructor, we can be certain that every wrapped piece of data will have a
unique name. At this point we have a safe way for users to create proofs about
their data and a safe way of library writers to make assumptions that the user
can't ignore.

Conclusion
==========

Ghosts of Departed Proofs goes a little farther than this and it's totally worth
reading, but for now we'll say that this concept of naming things and using
smart constructors is good enough for us.
