{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE QuantifiedConstraints #-}

module Rust where

import Env
import HList
import Prelude hiding ((>>=), (>>))
import Control.Monad.Indexed

-- | A reference can either be shared or exclusive.
data RefType = Exclusive | Shared

-- |A value can either be mutable or immutable.
data OwnType = Mutable | NotMutable

-- |If you hold an owner instance, it means you own whatever has the name s'
data Owner (rt :: OwnType) s' a = Owner

-- |If you hold a reference, it means you can use whatever has the name s'
data Reference (rt :: RefType) s s' a = Reference

-- |A value on our "heap" which has "address" s'
newtype Value s' a = Value {unOwn :: a}

-- |I make a lot of these helper types. Maybe there is a better way to create
-- them?
type WithSRef il ol s s' a rt = Env il (Reference rt s s' a ': ol) (Reference rt s s' a)

-- |Given a value you own, you can create a reference to it assuming there
-- aren't any exclusive references already in existence.
mkRef :: (Find (Value s' a) il ~ i, forall rs. NotIsIn (Reference Exclusive rs s' a) il) => Owner ot s' a -> WithSRef il il (forall s. s) s' a Shared
mkRef _ = track Reference >> ireturn Reference

-- |You can also make mutable references with a similar requirement.
mkMutRef :: (Find (Value s' a) il ~ i, forall rs rt. NotIsIn (Reference rt rs s' a) il) => Owner Mutable s' a -> WithSRef il il (forall s. s) s' a Exclusive
mkMutRef _ = track Reference >> ireturn Reference


-- |If you have a reference, you can duplicate it without fear as long as the
-- lifetimes work out.
dupRef :: (Find (Value s' a) il ~ i) => Reference Shared s1 s' a -> WithSRef il il (forall s. s) s' a Shared
dupRef _ = track Reference >> ireturn Reference

-- |Dereference a pointer. This is called unsafe because it is no longer
-- managed by our environment.
unsafeDeref :: forall s s' a il i ol. (Find (Value s a) il ~ i, ResortHList il (Value s a) i) => Reference _ s' s a -> Env il (Value s a ': NewOut il i) a
unsafeDeref _ = releaseAny @(Value s a) >>= \v -> track v >> ireturn (unOwn v)

-- |Similar to the above but for owned values.
unsafeUnOwn :: forall s a il i ol rt. (Find (Value s a) il ~ i, ResortHList il (Value s a) i) => Owner rt s a -> Env il (NewOut il i) a
unsafeUnOwn _ = imap unOwn $ releaseAny @(Value s a)


-- TODO Yikes
plus :: (Find (Value s Int) il ~ i, Find (Value s' Int) (NewOut il i) ~ i', ResortHList il (Value s Int) i, ResortHList (NewOut il i) (Value s' Int) i')
     => Owner rt s Int -> Owner rt2 s' Int -> (WithSPlus rt' il (NewOut (NewOut il i) i') (forall s3. s3) (Int))
plus oa ob = do
    a <- unsafeUnOwn oa
    b <- unsafeUnOwn ob
    let' $ a+b


type WithS il ol s a = Env il (Value s a ': ol) (Owner NotMutable s a)
type WithSPlus m il ol s a = Env il (Value s a ': ol) (Owner m s a)
type WithSLet m il s a = Env il (Value s a ': il) (Owner m s a)
type WithSCopy m il ol s s' a = Env il (Value s' a ': Value s a ': ol) (Owner m s' a)
type WithSMut il ol s a = Env il (Value s a ': ol) (Owner Mutable s a)
type WithSSet il ol s a = Env il (Value s a ': ol) ()

-- |Creates a new managed variable.
let' :: forall m a il. a -> (WithSLet m il (forall s. s) a)
let' a = track (Value a) >> ireturn Owner

-- |Copies a variable so that you can keep your current one alive while still
-- giving it to someone else.
copy :: forall m' m a il s i. (Find (Value s a) il ~ i, ResortHList il (Value s a) i) => Owner m s a -> (WithSCopy m' il (NewOut il i) s (forall s'. s') a)
copy _ = releaseAny @(Value s a) >>= (\(Value v) -> track (Value v) >> track (Value v)) >> ireturn Owner


-- |Moves an owned variable somewhere else. If there are any active references,
-- this will fail.
move :: forall il i ol a s rt. (Find (Value s a) il ~ i, forall rs rt. NotIsIn (Reference rt rs s a) il, ResortHList il (Value s a) i) => Owner rt s a -> (forall s'. WithS il (NewOut il i) s' a)
move a = releaseAny @(Value s a) >>= (track . Value . unOwn) >> ireturn Owner

-- |Modifies a mutable variable.
(=.) :: forall s a il i i'. (Find (Value s a) il ~ i, ResortHList il (Value s a) i) => Owner Mutable s a -> a -> WithSSet il (NewOut il i) s a
_ =. a = releaseAny @(Value s a) >> track (Value a) >> ireturn ()

-- |Drops a variable from scope. Normally, Rust would do this for you, but we
-- don't really have scope...
drop' :: forall s il i ol a rt. (Find (Value s a) il ~ i, forall rs rt. NotIsIn (Reference rt rs s a) il, ResortHList il (Value s a) i) => Owner rt s a -> Env il (NewOut il i) ()
drop' _ = releaseAny @(Value s a) >> ireturn ()

-- |Drops a reference from scope. I could probably make drop' polymorphic...but
-- this was easier.
dropRef' :: forall rt s s' il i ol a. (Find (Reference rt s s' a) il ~ i, ResortHList il (Reference rt s s' a) i) => Reference rt s s' a -> Env il (NewOut il i) ()
dropRef' _ = releaseAny @(Reference rt s s' a) >> ireturn ()
