module Nat where

data Nat = Z | S Nat
