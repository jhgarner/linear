{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PolyKinds #-}

module Env
    ( ResortHList
    , NewOut
    , Find
    , Env ()
    , runEnv
    , track
    , release
    , releaseAny
    , (>>)
    , (>>=)
    ) where

import Control.Monad.Indexed
import Data.Kind
import Data.Proxy
import HList
import Nat
import Prelude hiding ((>>=), (>>))

-- |An indexed State Datatype. The variable "il" is the list of things stored
-- in the state and "ol" is the list of things that will soon be stored in the
-- state. "E" and "unE" are not exported so you cannot arbitrarily mess with
-- the state.
newtype Env il ol a =
    E {unE :: HList il -> (HList ol, a)}

newtype Named s a = Named {unname :: a}

-- |Runs a state that has no leftover pieces and doesn't expect any pieces to
-- already exist.
runEnv :: Env '[] '[] a -> a
runEnv = snd . flip unE HNil

-- |The only way to add a variable to the state.
track :: a -> Env il (a ': il) ()
track a = E \hl -> (a ::: hl, ())

-- |One of 2 ways to remove a variable from the state. this one will fail if
-- the variable you want to remove is not the most recently one tracked.
release :: a -> Env (a ': il) il a
release a = E \(a ::: il) -> (il, a)

-- |Releases a variable anywhere in the stack. Despite the crazy type
-- signature, this function is actually the easier one to use. The only problem
-- is error messages can be terrifying. It also acts almost like a sink for
-- error messages. When somethnig goes wrong, GHC will probably decide it's
-- this function's fault.
releaseAny :: forall a il i ol. (Find a il ~ i, NewOut il i ~ ol, ResortHList il a i) => Env il ol a
releaseAny = E \ls -> popAny ls (Proxy :: Proxy a) (Proxy :: Proxy i)


instance IxFunctor Env where
    imap f (E wa) = E \hl -> f <$> wa hl

instance IxPointed Env where
    ireturn a = E \hl -> (hl, a)

instance IxApplicative Env where
    iap (E wf) (E wa) =
        E \hl ->
            let (fhl, f) = wf hl
                (ahl, a) = wa fhl
             in (ahl, f a)

instance IxMonad Env where
    ibind f (E wa) = E \hl ->
        let (ahl, a) = wa hl
            (E wb) = f a
         in wb ahl


-- |Exported for use with RebindableSyntax
(>>=) :: IxMonad m => m i j a -> (a -> m j k b) -> m i k b
(>>=) = flip ibind

-- |Exported for use with RebindableSyntax
(>>) :: IxMonad m => m i j a -> m j k b -> m i k b
a >> b = a >>= const b
