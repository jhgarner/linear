{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

module File where

import Env
import Prelude hiding ((>>=), (>>))
import Control.Monad.Indexed

-- |A file. More things could be added to make it more interesting.
newtype File s = File String

-- |A file handle is just something to toss onto the state stack. The type "s"
-- should be the same as the File.
newtype FileHandle s = FileHandle ()

-- |Creates a handle and File with the correct type. Whatever type they have
-- will be identical but won't be identical to anythig else.
type WithS il s = Env il (FileHandle s ': il) (File s)

-- |Opens a file and adds it to the environment.
openFile :: String -> (WithS il (forall s. s))
openFile s = track (FileHandle ()) >> ireturn (File s)

-- |Closes a file no matter where it is in the environment. The "~ i" thing is
-- just used as a way to bind a variable to the name "i".
closeFile :: forall s il i ol. (Find (FileHandle s) il ~ i, ResortHList il (FileHandle s) i) => File s -> Env il (NewOut il i) ()
closeFile _ = releaseAny @(FileHandle s) >> ireturn ()

-- |Reads a file no matter where it is in the environment.
readFile :: forall s il i ol. (Find (FileHandle s) il ~ i, ResortHList il (FileHandle s) i) => File s -> Env il (FileHandle s ': NewOut il i) String
readFile _ = releaseAny >>= track >> ireturn "t"
