{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeApplications #-}

module Lib
    ( someFunc
    ) where

import Control.Monad.Indexed
import Prelude hiding ((>>=), (>>), fail, readFile)
import Env
import File
import Rust

envExample :: Env '[] '[] String
envExample = do
    f1 <- openFile "test.txt"
    f2 <- openFile "testt.txt"

    c1 <- readFile f2
    c1 <- readFile f2
    c2 <- readFile f1


    c2 <- readFile f1
    c2 <- readFile f1

    closeFile $ f2
    closeFile $ f1
    -- closeFile $ f2

    ireturn $ c1 ++ c2

rustExample :: Env '[] '[] Int
rustExample = do
    a <- let' @NotMutable 5
    b <- let' @Mutable 6
    b =. 7
    -- a =. 7
    copied_b <- copy b
    c <- plus a copied_b
    drop' b
    d <- move c
    -- drop' $ a
    -- drop' $ b
    -- drop' $ c
    rd <- mkRef $ d
    rc <- mkRef $ d
    -- rmc <- mkMutRef $ d
    dropRef' rc
    dropRef' rd
    -- move d
    unsafeUnOwn d
    -- v <- unsafeDeref rc
    -- drop' d
    -- release c
    -- release rc
    -- ireturn "t"
    -- unsafeUnOwn c

someFunc :: IO ()
someFunc = print $ runEnv rustExample
