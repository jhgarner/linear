{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}

module HList where

import Data.Kind
import Data.Proxy
import Nat

-- |A heterogeneous list. Unlike a normal list in Haskell, you can store any
-- number of different types in it. That type parameter "l" tells you what's in
-- the list. The Type at the head of "l" will be the type of the variable at
-- the head of our HList.
data HList (l :: [Type]) :: Type where
    (:::) :: a -> HList ls -> HList (a ': ls)
    HNil :: HList '[]

-- |This typeclass exists so I can pretend like I'm pattern matching on Types.
-- PopAny is the interesting function. The type "ls" is the list of types in
-- our HList. The type "a" is the type we want to pop out of the list. The type
-- "n" tells us how close we are to "a". When "n" is 0, we've found it. This
-- entire way of coding is super unintuitive.
class ResortHList (ls :: [Type]) (a :: Type) (n :: Nat) where
    -- |The types stored in the HList after calling popAny.
    type family NewOut ls n :: [Type]

    -- |The only interesting parameter is the first one. The others exist to
    -- satisfy GHC.
    popAny :: HList ls -> Proxy a -> Proxy n -> (HList (NewOut ls n), a)

-- |This instance is like pattern matching on the type when the head of our
-- list has the type we're looking for. We say that has "n == Z" because this
-- should be the end of our countdown. This is our base case.
instance ResortHList (a ': as) a Z where
    -- |In this case, our output is just the tail of our list.
    type instance NewOut (a ': as) Z = as

    popAny (a ::: as) _ _ = (as, a)

-- |This instance matches the case when the type we're looking for is somewhere
-- is deeper in the list.
instance (ResortHList as b n) => ResortHList (c ': as) b (S n) where
    type instance NewOut (c ': as) (S n) = c ': NewOut as n

    popAny (a ::: as) pb pn = let (hl, b) = popAny as pb (Proxy :: Proxy n)
                               in (a ::: hl, b)

-- |This type level function tells you how long you have until you reach what
-- you're looking for. It works super well with the ResortHList "function".
type family Find (a :: k) (as :: [k]) :: Nat where
    Find a (a ': _) = Z
    Find a (b ': bs) = S (Find a bs)

type family IsEq (a :: k) (b :: k) :: Bool where
    IsEq a a = True
    IsEq a b = False
class HEq (a :: k) (b :: k) (c :: Bool) | a b -> c where
instance HEq a a 'True
instance (bool ~ False) => HEq a b bool

class NotIsIn (a :: k) (as :: [k]) where

instance NotIsIn a '[]
-- instance IsIn a (a ': as) 'True
instance (NotIsIn a bs, IsEq a b ~ 'False) => NotIsIn a (b ': bs)
